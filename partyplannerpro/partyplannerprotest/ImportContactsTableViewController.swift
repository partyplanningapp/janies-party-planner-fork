//
//  ChecklistTableViewController.swift
//  Party Planner Pro
//
//  Created by Janie on 10/18/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class ImportContactsTableViewController: UITableViewController {
    
    var SelectedParty:String!
    var SelectedColor:Int!
    var items: [Guest] = []
    
    var guest=[String]()
    var guestSub = [String]()
    var guestText: [String:String] = [:];
    var guestSubtext: [String:String] = [:];
    var guestChecked:[String:Int] = [:];
    var guestId:[String:Int] = [:];
    
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let newItem = Guest()
        newItem.name = "John Doe"
        newItem.phone = "555555555"
        newItem.checked = false
        items.append(newItem)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("This is the selectedParty in guestchecklist:" , SelectedParty)
        
        self.tableView.backgroundColor = UIColor(red: 122/255, green: 0/255, blue: 89/255, alpha: 1.0)
        
        //open Database
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("partyplannerv4.db")
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        
        if sqlite3_exec(db, "create table if not exists guests(id integer primary key autoincrement, noteId integer, text varchar, subtex varchar, checked boolean, foreign key(noteId) references notes(id))",nil,nil,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
        
        //query database
        _ = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, "select * from guests", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        
//        let myNewerInt = Int32(SelectedParty!)
//        
//        if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("error binding name: \(errmsg)")
//        }
        
        
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let id = sqlite3_column_int64(statement, 0)
            let partyId = sqlite3_column_int64(statement, 1)
            let theGuestText = sqlite3_column_int64(statement, 2)
            let theGuestSubtext = sqlite3_column_int64(statement, 3)
            let checked = sqlite3_column_int64(statement, 4)
            
            print("id = \(id); ", terminator: "")
            print("partyId = \(partyId); ", terminator: "")
            
            if let name = sqlite3_column_text(statement, 2)
            {
                let nameString = String(cString: name)
                
                guest.append(nameString)
                guestText[nameString] = String(theGuestText)
                //guestSubtext[nameString] = String(theGuestSubtext)
                guestChecked[nameString] = 0
                guestId[nameString] = Int(id)
                
                //                var guest=[String]()
                //                var guestText: [String: String] = [:];
                //                var guestSubtext: [String:String] = [:];
                //                var guestChecked:[String:Int] = [:];
                //                var guestId:[String:Int] = [:];
                
                print("note name = \(nameString)")
            } else {
                print("name not found")
            }
            
            if let subname = sqlite3_column_text(statement, 3)
            {
                let nameString = String(cString: subname)
                
                self.guestSub.append(nameString)
                
                self.tableView.reloadData()
                
            }
            
            
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        statement = nil
        
        //print("I am noteList in checklist: ", noteList)
        
        self.tableView.reloadData()
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return guest.count
        }
        else
        {
            return 1//number of color accents
        }
    }
    
    func configureCheckmarkForCell(cell: UITableViewCell, index: Int) {
        let isChecked = items[index].checked
        if (isChecked) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
    }
    
    //Manages text in each individual cell
    func configureTextForCell(cell: UITableViewCell, item: Guest) {
        
        //a text label and a detail label
        let guestname = cell.textLabel
        let guestphone = cell.detailTextLabel
        guestname?.text = item.name
        guestphone?.text = item.phone
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuestItem", for: indexPath)
        let theIndexPath = indexPath.item
        let theSecondIndexPath = indexPath.item
        
        
        cell.textLabel?.text = guest[theIndexPath]
//        
       cell.detailTextLabel?.text = guestSub[theIndexPath]
        
        cell.textLabel?.textColor = UIColor.white
        let checked = guestChecked[(cell.textLabel?.text!)!]
        
        print("checked is: " , checked)
        
        if checked == 0 || checked == nil
        {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
            
        else
        {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            
        }
        
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        cell.detailTextLabel?.textColor = UIColor.white
        
        return cell
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell: UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        
        let checked = guestChecked[cell.textLabel!.text!]
        if (checked == 0)
        {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            guestChecked[(cell.textLabel?.text)!] = 1
            let theName = cell.textLabel!.text
            
            print ("I am theName", theName)
            
            //            let theId = noteList[theName]
            
            let fileURL = try! FileManager.default.url(for:.documentDirectory, in: .userDomainMask,appropriateFor: nil, create: false).appendingPathComponent("partyplannerv4.db")
            var db: OpaquePointer? = nil
            if sqlite3_open(fileURL.path, &db) != SQLITE_OK
            {
                print("error opening database")
            }
            
            
            //insert
            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
            var statement: OpaquePointer? = nil
            
            let nametext = cell.textLabel?.text
            let phonetext = cell.detailTextLabel?.text
            
            
            if sqlite3_prepare_v2(db, "insert into guests(noteId,text,subtex,checked) values(?,?,?,?)", -1, &statement, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error prepping statement: \(errmsg)")
            }
            
            let myNewInt = self.SelectedParty
            let myNewerInt = Int32(myNewInt!)
            ///bind as many times as you have variables going into table
            //important parts are statemnt, second integer, and name
            if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding newer int: \(errmsg)")
            }
            
            if sqlite3_bind_text(statement, 2, nametext , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            if sqlite3_bind_text(statement, 3, phonetext , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding phone: \(errmsg)")
            }
            
            if sqlite3_bind_int(statement, 4, 0) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding boolean: \(errmsg)")
            }

            
            
            //            //only one step
            if sqlite3_step(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error stepping: \(errmsg)")
            }
            
            
            //only have one finalize
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing: \(errmsg)")
            }
            
            //reset statement
            statement = nil
            
            
            
        }
        
    }
    
    
    
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    
    
    // Override to support editing the table view.
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            let cell: UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
//            
//            // Delete the row from the data source
//            let name = guest[indexPath.row]
//            let theId = guestId[cell.textLabel!.text!]
//            
//            guest.remove(at: indexPath.row)
//            guestSub.remove(at: indexPath.row)
//            
//            
//            
//            tableView.deleteRows(at: [indexPath], with: .fade)
//            //remove from DB
//            
//            
//            //open Database
//            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//                .appendingPathComponent("partyplannerv2.db")
//            
//            
//            
//            var db: OpaquePointer? = nil
//            if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
//                print("error opening database")
//            }
//            
//            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
//            var statement: OpaquePointer? = nil
//            
//            if sqlite3_prepare_v2(db, "delete from guests where id=(?)", -1, &statement, nil) != SQLITE_OK {
//                let errmsg = String(cString: sqlite3_errmsg(db))
//                print("error preparing select: \(errmsg)")
//            }
//            
//            let myNewerInt = Int32(theId!)
//            ///bind as many times as you have variables going into table
//            //important parts are statemnt, second integer, and name
//            if sqlite3_bind_int(statement, 1, myNewerInt) != SQLITE_OK {
//                let errmsg = String(cString: sqlite3_errmsg(db))
//                print("error binding id: \(errmsg)")
//            }
//            
//            //one step
//            if sqlite3_step(statement) != SQLITE_DONE {
//                let errmsg = String(cString: sqlite3_errmsg(db))
//                print("failure deleting party: \(errmsg)")
//            }
//            
//            
//            //finalize & reset statement
//            if sqlite3_finalize(statement) != SQLITE_OK {
//                let errmsg = String(cString: sqlite3_errmsg(db))
//                print("error finalizing prepared statement: \(errmsg)")
//            }
//            
//            statement = nil
//            
//            //close db & set to nil
//            if sqlite3_close(db) != SQLITE_OK {
//                print("error closing database")
//            }
//            
//            db = nil
//            
//            
//            //close db & set to nil
//            if sqlite3_close(db) != SQLITE_OK {
//                print("error closing database")
//            }
//            
//            db = nil
//            
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//        }
//    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
