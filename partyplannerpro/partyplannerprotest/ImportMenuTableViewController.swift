//
//  ChecklistTableViewController.swift
//  Party Planner Pro
//
//  Created by Janie on 10/23/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class ImportMenuTableViewController: UITableViewController {
    
    //var SelectedParty:String!
    var SelectedNote:String!
    var SelectedColor:Int!
    
    var notes=[String]()
    var noteList: [String: Int] = [:];
    var noteId:[String:Int] = [:];
    
    
    var items: [ChecklistItem] = []
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let newItem = ChecklistItem()
        newItem.name = "Item Name"
        newItem.checked = false
        items.append(newItem)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print("The Selected Party is:", SelectedParty)
        //print("The Selected note is:", SelectedNote)
        
        self.tableView.backgroundColor = UIColor(red: 122/255, green: 0/255, blue: 89/255, alpha: 1.0)
        
        
        //open Database
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("partyplannerv4.db")
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        
        if sqlite3_exec(db, "create table if not exists content(id integer primary key autoincrement, noteId integer, text varchar, checked boolean, foreign key(noteId) references notes(id))",nil,nil,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
        
        //query database
        _ = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, "select * from content", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        
        let myNewerInt = Int32(SelectedNote!)
        
        if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error binding name: \(errmsg)")
        }
        
        
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let id = sqlite3_column_int64(statement, 0)
            let partyId = sqlite3_column_int64(statement, 1)
            let checked = sqlite3_column_int64(statement, 3)
            
            print("id = \(id); ", terminator: "")
            print("partyId = \(partyId); ", terminator: "")
            
            if let name = sqlite3_column_text(statement, 2)
            {
                let nameString = String(cString: name)
                
                notes.append(nameString)
                noteList[nameString]=0
                noteId[nameString]=Int(id)
                
                print("note name = \(nameString)")
            } else {
                print("name not found")
            }
            
            
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        statement = nil
        
        //print("I am noteList in checklist: ", noteList)
        
        self.tableView.reloadData()
        
        
        print(noteList)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
       
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return notes.count
        }
        else
        {
            return 1//number of color accents
        }
    }
    /*
     func configureCheckmarkForCell(cell: UITableViewCell, index: Int) {
     let isChecked = items[index].checked
     if (isChecked) {
     cell.accessoryType = UITableViewCellAccessoryType.checkmark
     } else {
     cell.accessoryType = UITableViewCellAccessoryType.none
     }
     }
     
     //Manages text in each individual cell
     
     func configureTextForCell(cell: UITableViewCell, item: ChecklistItem)
     {
     let label: UILabel = cell.viewWithTag(100) as! UILabel
     label.text = item.name
     label.textColor = UIColor.white
     }
     */
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Item", for: indexPath)
        /*
         let item: ChecklistItem = notes[indexPath.row]
         configureTextForCell(cell: cell, item: item)
         configureCheckmarkForCell(cell: cell, index: indexPath.row)
         */
        
        cell.textLabel?.text = notes[indexPath.item]
        
        cell.textLabel?.textColor = UIColor.white
        let checked = noteList[(cell.textLabel?.text!)!]
        
        print("checked is: " , checked)
        
        if checked == 0
        {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
            
        else
        {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            
        }
        
        cell.backgroundColor = UIColor.clear
        
        
        return cell
    }
    
    /*
     override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     
     tableView.reloadData()
     }
     */
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        // let item: ChecklistItem = items[indexPath.row]
        // item.checked = !item.checked
        
        //let isChecked = items[index].checked
        
        //connects and opens db
        let fileURL = try! FileManager.default.url(for:.documentDirectory, in: .userDomainMask,appropriateFor: nil, create: false).appendingPathComponent("partyplannerv4.db")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK
        {
            print("error opening database")
        }
        
        
        if sqlite3_exec(db, "create table if not exists content(id integer primary key autoincrement, noteId integer, text varchar, checked boolean, foreign key(noteId) references notes(id))",nil,nil,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
        let checked = noteList[cell.textLabel!.text!]
        if (checked == 0)
        {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            noteList[(cell.textLabel?.text)!] = 1
            let userText = cell.textLabel!.text
            
            //insert
            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
            
            var statement: OpaquePointer? = nil
            
            if sqlite3_prepare_v2(db, "insert into content(noteId,text,checked) values(?,?,?)", -1, &statement, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error prepping statement: \(errmsg)")
            }
            
            let myNewInt = self.SelectedNote
            let myNewerInt = Int32(myNewInt!)
            ///bind as many times as you have variables going into table
            //important parts are statemnt, second integer, and name
            if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            if sqlite3_bind_text(statement, 2, userText , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
            }
            
            if sqlite3_bind_int(statement, 3, 0) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding boolean: \(errmsg)")
            }
            
            
            //only one step
            if sqlite3_step(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error stepping: \(errmsg)")
            }
            
            
            //only have one finalize
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing: \(errmsg)")
            }
            
            //reset statement
            statement = nil
        }
        
    }
    
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
//    // Override to support editing the table view.
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            let cell: UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
//            
//            let name = notes[indexPath.row]
//            let theId = noteId[cell.textLabel!.text!]
//            notes.remove(at: indexPath.row)
//            
//            tableView.deleteRows(at: [indexPath], with: .fade)
//            //remove from DB
//            
//            
//            //open Database
//            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//                .appendingPathComponent("partyplannerv4.db")
//            
//            
//            
//            var db: OpaquePointer? = nil
//            if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
//                print("error opening database")
//            }
//            
//            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
//            var statement: OpaquePointer? = nil
//            
//            if sqlite3_prepare_v2(db, "delete from content where id=(?)", -1, &statement, nil) != SQLITE_OK {
//                let errmsg = String(cString: sqlite3_errmsg(db))
//                print("error preparing select: \(errmsg)")
//            }
//            
//            
//            let myNewerInt = Int32(theId!)
//            ///bind as many times as you have variables going into table
//            //important parts are statemnt, second integer, and name
//            if sqlite3_bind_int(statement, 1, myNewerInt) != SQLITE_OK {
//                let errmsg = String(cString: sqlite3_errmsg(db))
//                print("error binding id: \(errmsg)")
//            }
//            
//            //one step
//            if sqlite3_step(statement) != SQLITE_DONE {
//                let errmsg = String(cString: sqlite3_errmsg(db))
//                print("failure deleting party: \(errmsg)")
//            }
//            
//            
//            //finalize & reset statement
//            if sqlite3_finalize(statement) != SQLITE_OK {
//                let errmsg = String(cString: sqlite3_errmsg(db))
//                print("error finalizing prepared statement: \(errmsg)")
//            }
//            
//            statement = nil
//            
//            //close db & set to nil
//            if sqlite3_close(db) != SQLITE_OK {
//                print("error closing database")
//            }
//            
//            db = nil
//            
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//        }
//    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
