//
//  Note.swift
//  partyplannerprotest
//
//  Created by Ariba Siddiqui on 11/12/16.
//  Copyright © 2016 software2. All rights reserved.
//

import Foundation

class Note {
    var title = ""
    var content = ""
}
