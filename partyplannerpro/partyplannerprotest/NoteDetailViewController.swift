//
//  NoteDetailViewController.swift
//  partyplannerprotest
//
//  Created by Ariba Siddiqui on 11/12/16.
//  Copyright © 2016 software2. All rights reserved.
//

import UIKit

class NoteDetailViewController: UIViewController {
    
    //let defaults = UserDefaults.standard
    
    var SelectedParty:String!
    var SelectedColor:Int!
    var SelectedNote:String!
    var TheUserText:String!
    var selectedId = 0;
    var goFoward = true;
    
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var contentTextField: UITextView!
    
    override func viewDidLoad() {
        
        print("The User Text:", TheUserText)
        titleTextField.text = TheUserText

        
        
        switch SelectedColor
        {
        case 0:
            self.view.backgroundColor = UIColor(red: 122/255, green: 0/255, blue: 89/255, alpha: 1.0)
            titleTextField.backgroundColor = UIColor(red: 122/255, green: 0/255, blue: 89/255, alpha: 1.0)
            contentTextField.backgroundColor = UIColor(red: 122/255, green: 0/255, blue: 89/255, alpha: 1.0)
        case 1:
            self.view.backgroundColor = UIColor(red: 0/255, green: 202/255, blue: 216/255, alpha: 1.0)
            titleTextField.backgroundColor = UIColor(red: 0/255, green: 202/255, blue: 216/255, alpha: 1.0)
            contentTextField.backgroundColor = UIColor(red: 0/255, green: 202/255, blue: 216/255, alpha: 1.0)
        case 2:
            self.view.backgroundColor = UIColor(red: 155/255, green: 0/255, blue: 2/255, alpha: 1.0)
            titleTextField.backgroundColor = UIColor(red: 155/255, green: 0/255, blue: 2/255, alpha: 1.0)
            contentTextField.backgroundColor = UIColor(red: 155/255, green: 0/255, blue: 2/255, alpha: 1.0)
        case 3:
            self.view.backgroundColor = UIColor(red: 255/255, green: 232/255, blue: 193/255, alpha: 1.0)
            titleTextField.backgroundColor = UIColor(red: 255/255, green: 232/255, blue: 193/255, alpha: 1.0)
            contentTextField.backgroundColor = UIColor(red: 255/255, green: 232/255, blue: 193/255, alpha: 1.0)        default:
            break;
        }
        if(SelectedColor == 3)
        {
            
            contentTextField.textColor  = UIColor(red: 214/255, green: 182/255, blue: 9/255, alpha: 1.0)
        }
        else
        {
            contentTextField.textColor = UIColor.black
        }
        super.viewDidLoad()
        
        
        //open Database
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("partyplannerv4.db")
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }

        
        
        if sqlite3_exec(db, "create table if not exists guests(id integer primary key autoincrement, noteId integer, text varchar, subtex varchar, checked boolean, foreign key(noteId) references notes(id))",nil,nil,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
        
        //query database
        let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, "select * from guests where text = (?) ", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        
        let myNewerInt = Int32(SelectedNote!)
        
//        if sqlite3_bind_int(statement, 1, myNewerInt!) != SQLITE_OK {
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("error binding name: \(errmsg)")
//        }
        
        if sqlite3_bind_text(statement, 1, TheUserText, -1, SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("failure binding name delete: \(errmsg)")
        }
        
        if sqlite3_step(statement) != SQLITE_OK {
            let id = sqlite3_column_int64(statement, 0)
            let noteId = sqlite3_column_int64(statement, 1)
            let title = sqlite3_column_text(statement, 2)
            let text = sqlite3_column_text(statement, 3)
            
            
            print("id = \(id); ", terminator: "")
            
            if let name = sqlite3_column_text(statement, 2)
            {
                
                //let titleString = String(cString: title!)
                let subtextString = String(cString: text!)
                print("This is the subjextString:", subtextString)
//                titleTextField.text = titleString
                
                contentTextField.text = subtextString
                
                let nameString = String(cString: name)
               

                selectedId = Int(noteId)
            
                
                print("note name = \(nameString)")
            } else {
                print("name not found")
            }
            
            
            
            if sqlite3_finalize(statement) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error finalizing prepared statement: \(errmsg)")
            }
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //titleTextField.text = defaults.object(forKey: "title") as! String?
        //contentTextField.text = defaults.object(forKey: "content") as! String!
        
        //titleTextField.text = note.title
        //contentTextField.text = note.content
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
//        note.title = titleTextField.text
//
//        note.content = contentTextField.text
//        
//        defaults.set(note.title, forKey: "title")
//        defaults.set(note.content, forKey: "content")

        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("partyplannerv4.db")
        
        let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        
        if sqlite3_exec(db, "create table if not exists freeform(id integer primary key autoincrement, noteId integer, title varchar, text varchar, FOREIGN KEY(noteId) REFERENCES notes(id))",nil,nil,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error creating table: \(errmsg)")
        }
        
        
        //query database
        _ = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, "update guests set subtex=(?) where text=(?)", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        let userTitle = titleTextField.text
        print("This is user title", userTitle)
        let userText = contentTextField.text
        print("this is the body text", userText)
        
        let myNewerInt = Int32(SelectedNote)
        

        
        if sqlite3_bind_text(statement, 1, userText , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error binding name: \(errmsg)")
        }
        
//        if sqlite3_bind_int(statement, 2, myNewerInt!) != SQLITE_OK {
//            let errmsg = String(cString: sqlite3_errmsg(db))
//            print("error binding name: \(errmsg)")
//        }
        
        if sqlite3_bind_text(statement, 2, TheUserText , -1 , SQLITE_TRANSIENT) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db))
                print("error binding name: \(errmsg)")
        }
        
        if sqlite3_step(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error stepping: \(errmsg)")
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
     
        }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

